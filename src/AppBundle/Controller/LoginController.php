<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 02/12/2017
 * Time: 18:46
 */

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use AppBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /**
     * @Route("/connexion", name="login")
     */
    public function indexAction(){
        $authenticationUtils = $this->get('security.authentication_utils');
        return $this->render('login/login.html.twig', [
            'lastUsername' => $authenticationUtils->getLastUsername(),
            'errors' => $authenticationUtils->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request) {
        $user=new User();
        $form=$this->createForm(UserType::class,$user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $encoder = $this->get('security.password_encoder');
            $encode_password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encode_password);
            $em=$this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('login');
        }
        return $this->render('login/register.html.twig',[
            'registerForm'=>$form->createView()
        ]);
    }

}