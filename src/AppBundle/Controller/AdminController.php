<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 04/12/2017
 * Time: 18:36
 */

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use AppBundle\Form\Type\UserEditType;
use AppBundle\Form\Type\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     */
    public function indexAction(){
        $em = $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();
        return $this->render('admin/index.html.twig',[
            'users' => $users
        ]);
    }

    /**
     * @Route("user/{id}", requirements={"id":"\d+"}, name="update_user")
     * @Method("GET|POST")
     */
    public function updateUserAction(Request $request, User $user){
        $form = $this->createForm(UserEditType::class, $user);
        $referer = $request->headers->get('referer');

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirect($referer);
        }

        return $this->render('user/profil.html.twig', [
            'userForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/{id}/delete_user", name="delete_user")
     * @Method("GET")
     */
    public function deleteUserAction(Request $request, User $user) {
        $referer = $request->headers->get('referer');
        //Suppression de l'artiste dans la base de données
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        return $this->redirect($referer);
    }
}