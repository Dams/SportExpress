<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 16/11/2017
 * Time: 13:45
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="Materiel")
 */
class Materiel
{
    use idTrait;
    /**
     * @ORM\Column()
     * @Assert\Type("String")
     * @Assert\Length(max="255")
     */
    private $nom;
    /**
     * @ORM\Column()
     * @Assert\Type("String")
     * @Assert\Length(max="1000")
     */
    private $description;
    /**
     * @ORM\Column()
     * @Assert\Type("Int")
     */
    private $prix;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TypeMateriel", inversedBy="materiels")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Image", mappedBy="materiel")
     */
    private $image;

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * @param mixed $prix
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

}
