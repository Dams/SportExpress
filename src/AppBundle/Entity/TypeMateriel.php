<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 17/11/2017
 * Time: 14:37
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="TypeMateriel")
 */
class TypeMateriel
{
    use idTrait;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Materiel", mappedBy="type")
     */
    private $materiels;
    /**
     * @ORM\Column()
     * @Assert\Type("String")
     * @Assert\Length(max="255")
     */
    private $nom;

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }
}
