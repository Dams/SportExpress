<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 27/11/2017
 * Time: 14:30
 */
namespace AppBundle\Entity;

trait idTrait
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}