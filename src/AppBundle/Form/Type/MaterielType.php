<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 01/12/2017
 * Time: 10:23
 */

namespace AppBundle\Form\Type;


use AppBundle\Entity\TypeMateriel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaterielType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, ['label' => 'Nom du produit'])
            ->add('description', TextareaType::class, ['label' => 'Description'])
            ->add('prix', IntegerType::class, ['label' => 'Prix'])
            ->add('type',EntityType::class,array(
                'class' => TypeMateriel::class,
                'choice_label' => 'nom'
            ))
            ->add('image',FileType::class, ['label' => 'Ajouter une image', 'required'=>false]);
    }

    /**
     * (@inheritdoc)
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Materiel'
        ));
    }


}