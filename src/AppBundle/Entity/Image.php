<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 04/12/2017
 * Time: 10:16
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="Image")
 *
 */
class Image
{
    use idTrait;
    /**
     * @ORM\Column()
     * @Assert\Type("String")
     * @Assert\Length(max="255")
     */
    private $nom;
    /**
     * @ORM\Column()
     * @Assert\Type("String")
     */
    private $chemin;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Materiel", inversedBy="image")
     */
    private $materiel;

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getChemin()
    {
        return $this->chemin;
    }

    /**
     * @param mixed $path
     */
    public function setChemin($chemin)
    {
        $this->chemin = $chemin;
    }

    /**
     * @return mixed
     */
    public function getMateriel()
    {
        return $this->materiel;
    }

    /**
     * @param mixed $materiel
     */
    public function setMateriel($materiel)
    {
        $this->materiel = $materiel;
    }



}
