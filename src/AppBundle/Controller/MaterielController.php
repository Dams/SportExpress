<?php
/**
 * Created by PhpStorm.
 * User: Damian
 * Date: 17/11/2017
 * Time: 14:44
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Materiel;
use AppBundle\Entity\TypeMateriel;
use AppBundle\Form\Type\MaterielType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class MaterielController extends Controller
{
    /**
     * @Route("/materiel", name="materiel")
     * @Method("GET|POST")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $materiel = $em->getRepository(Materiel::class)->findAll();
        return $this->render('materiel/materiel.html.twig',[
            'materiel' => $materiel
        ]);
    }
    public function findTypeAction()
    {
        //Comment récupérer l'idType ?

        $em = $this->getDoctrine()->getManager();
        //$materiel = $em->getRepository(Materiel::class)->findBy(
            //['type' => $numType];
        //);
    }

    /**
     * @Route("/materiel/creer", name="create_materiel")
     * @Method("GET|POST")
     */
    public function createAction(Request $request){
        //Récupération du Manager de Doctrine
        $materiel = new Materiel();

        $form = $this->createForm(MaterielType::class, $materiel);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() ) {
            $em=$this->getDoctrine()->getManager();
            $em->persist($materiel);
            $em->flush();
            return $this->redirectToRoute('homepage');
        }
        return $this->render('materiel/create.html.twig',[
            'materielForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/materiel/{id}", name="details_materiel")
     * @param Materiel $materiel
     */
    public function detailsAction(Materiel $materiel){
        return $this->render('materiel/details.html.twig',[
            'materiel' => $materiel
        ]);
    }
    /**
     * @Route("/materiel/update/{id}",name="update_materiel")
     */
   /* public function updateAction(Materiel $materiel) {

    }*/


}